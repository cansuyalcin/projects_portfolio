# Overview

The personal projects include various type of projects such as,

* The projects I have applied during my internships 

    * [Metal Surface Defect Detection](https://gitlab.com/cansuyalcin/projects_portfolio/-/tree/master/Personal%20Projects/Metal%20Surface%20Defect%20Detection) 
    
    * [Customer Churn Analysis](https://gitlab.com/cansuyalcin/projects_portfolio/-/tree/master/Personal%20Projects/Customer%20Churn%20Analysis)
    

* The projects on some specific topics I am specifically interested in,

    * [RFM analysis for Customer Segmentation](https://gitlab.com/cansuyalcin/projects_portfolio/-/tree/master/Personal%20Projects/RFM%20analysis%20for%20Customer%20Segmentation)
    
    * [Identifying Target Customer Profiles with Market Analysis](https://gitlab.com/cansuyalcin/projects_portfolio/-/tree/master/Personal%20Projects/Identifying%20Target%20Customer%20Profiles%20with%20Market%20Analysis)

    * [Face Recognition Systems](https://gitlab.com/cansuyalcin/projects_portfolio/-/tree/master/Personal%20Projects/Face%20Recognition)


