# Cansu Yalçın - Projects Portfolio

## Index

This repository holds all of my personal projects that I have done over the years. They are currently under two main categories.

1. [Cancer Bioinformatics Research](https://gitlab.com/cansuyalcin/projects_portfolio/-/tree/master/Cancer%20Bioinformatics%20Research "Research") 

2. [Personal Projects](https://gitlab.com/cansuyalcin/projects_portfolio/-/tree/master/Personal%20Projects "Projects") 

Every category has its own variety of projects and is accompanied by documentation.

## Background

Hi! This is Cansu Yalçın welcome to my Project Portfolio. Let me introduce myself. Currently, I am pursuing my bachelor's in industrial engineering and computer engineering (double major) at Antalya Bilim University and planning to graduate by June 2021. Throughout my studies, I have completed several internships on such content data science, data analytics, machine learning, deep learning, and computer vision. I have been also part of the computational biology research group in my university for more than a year. I am working on algorithms design to identify the significance of mutual exclusion and epistasis on genes across 8 different cancer subtypes. 

